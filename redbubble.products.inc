<?php
// $Id$

/** @file
 *
 */

/**
 * Load a single record.
 *
 * @param $id
 *    The id representing the record we want to load.
 */
function redbubble_load($id, $reset = FALSE) {

  return redbubble_load_multiple(array($id), $reset);
}

/**
 * This function returns a array of product records and has as input a array of AID numbers.
 */
function redbubble_load_multiple($ids = array(), $conditions = array(), $reset = FALSE) {

  return entity_load('rb_products', $ids, $conditions, $reset);
}

/**
 * This function saves a product array of information to the database table.
 * If the record already exists it will be over written. This is where the fields AID and CREATED are filled.
 */
function redbubble_save($product) {


//  print "<br />";
  watchdog('redbubble', "redbubble product update = <pre>" . print_r($product, true) . "</pre>");
//  print "<br />";

  $result = db_query("SELECT aid FROM {rb_products} WHERE productid=:proid", array(':proid' => $product->productid,
    ));
  $sid = $result->fetchField(0);
/*
  print_r($sid);
  print "\r\n";
  print_r($test[$sid]->productid);
  print "\r\n";
*/
  $product->created = REQUEST_TIME;
  $product->aid     = $sid;
  $test             = redbubble_load($sid);
  if ($test[$sid]->productid = $product->productid) {
    entity_delete('rb_products', $sid);
  }
  entity_save('rb_products', $product);
  return $products->aid;
}

/**
 * This function returns a product array. Below is the mapping table for the input of this function.
 */
function container_rbproducts($a, $b, $c, $d, $e, $f, $g, $h, $i, $j, $k, $l, $m, $n, $o, $p) {


  $container = array(
    'aid' => '',
    'title' => $a,
    'description' => $b,
    'popnum' => $c,
    'imageurl' => $d,
    'productid' => $e,
    'price' => $f,
    'pubdate' => $g,
    'artist' => $h,
    'sales_link' => $i,
    'color' => $j,
    'size' => $k,
    'ptype' => $l,
    'kwords' => $m,
    'custom' => $n,
    'style' => $o,
    'prodnumber' => $p,
    'created' => '',
  );
  $test = entity_create('rb_products', $container);

  return $test;
}

function get_all_redbubble($store) {



  $ass_id = variable_get('redbubble_associate_id', '');
/*
  print "\r\n";
  echo date("H:m:s");
  echo "\r\n";
*/
  $rss              = new lastRSS;
  $rss->CDATA       = 'content';
  $rss->items_limit = 0;
  $perpage          = 100;
  $popnum           = 0;
  $pagenum          = 0;

  $rssurl = "http://www.redbubble.com/people/" . $ass_id . "/shop/recent.atom";

  if ($rs = $rss->get($rssurl)) {
    watchdog('redbubble', 'redbubble store feed at ' . l($rssurl, $rssurl) . ' returned data.  Example: <br /><pre>' . print_r($rs, true) . '</pre>');
    // print_r($rs);
    $totalnum = $rs['opensearch:totalResults'];

    $totalpage = ceil($totalnum / $perpage);
  }
  // fatal error - no cached RSS, no socket
  else {}

  // test values
  $perpage = 100;
  $totalpage = 1;
/*
  print 'perpage = ' . $perpage . "\r\n";
  print 'totalpage = ' .$totalpage . "\r\n";
  print 'pagenum = ' .$pagenum . "\r\n";
*/
  // start while
  while ($pagenum < $totalpage) {

    // create new RSS URL
    $pagenum = $pagenum + 1;


    $rssurl = "http://www.redbubble.com/people/" . $ass_id . "/shop/recent.atom";

    // Get our RSS data
    // if from rss getting
    if ($rs = $rss->get($rssurl)) {
      // Iterate through the RSS items and extract their data
      foreach ($rs as $key => $val) {
        // we only care about the <item> nodes in this case
        if ($key == "items") {
          foreach ($val as $index => $value) {
            // getting the data in variables
            $title       = urldecode($value['title']);
            $description = htmlspecialchars_decode(str_replace('&amp;', '&', $value['content']), ENT_HTML5);
            $imageurl    = $value['g:image_link'];
            $extracted_id = explode('/', $value['id']);
            $productid   = $extracted_id[1];
            $price       = $value['g:price'];
            $pubdate     = $value['published'];
            $artist      = $value['author'];
            $link        = $value['via'];
            $color       = $value['g:color'];
            $size        = $value['g:size'];
            $ptype       = $value['g:product_type'];
            $kwords      = $value['category'];
            $cust        = $value['c:iscustomizable'];
            $style       = $value['g:style'];

            // extra work on variables
            $prodnum = substr($productid, 37);
            //$pubdate = substr($pubdate, 0, -4);
            $pubdate = strtotime($pubdate);
            $pubdate = date('o-m-d H:m:s', $pubdate);
            $popnum  = $popnum + 1;
            //print "$popnum===";
/*
            switch ($prodnum) {
              case 128:
                $ptype = "bumper sticker";
                break;

              case 137:
                $ptype = "card-Greeting-Note";
                break;

              case 153:
                $ptype = "photo sculpture";
                break;

              case 155:
                $ptype = "Pet Clothing";
                break;

              case 156:
                $ptype = "photo print";
                break;

              case 167:
                $ptype = "keds shoe";
                break;

              case 172:
                $ptype = "US Postage";
                break;

              case 176:
                $ptype = "iPad/iPhone Case";
                break;

              case 228:
                $ptype = "Poster";
                break;

              case 235:
                $ptype = "t-shirt";
                break;

              case 240:
                $ptype = "Business Cards";
                break;

              case 243:
                $ptype = "Photo Cards";
                break;

              case 245:
                $ptype = "rack card";
                break;
            }
*/
            // strip any html from the description
            //$description = preg_replace("/<[^>]+>/", '', $description);
            $title = preg_replace("/<[^>]+>/", '', $title);

            $product = container_rbproducts(
              $title,
              $description,
              $popnum,
              $imageurl,
              $productid,
              $price,
              $pubdate,
              $artist,
              $link,
              $color,
              $size,
              $ptype,
              $kwords,
              $cust,
              $style,
              $prodnum
            );

            // write node to database

            redbubble_save($product);

            //print "\r\n";
          }
          // end for each index / value
        }
        // end if item
      }
      // end foreach $rs
    }
    //end if from rss getting
    // fatal error - no cached RSS, no socket
    else {}
    // end else from rss getting
  }
  // end while
/*
  print "\r\n";
  echo date("H:m:s");
  print "\r\n";
*/
}

