<?php
// $Id$


/**
 * @file
 * Providing extra functionality for the redbubble UI via views.
 */
function redbubble_views_data() {
  $data['rb_products']['table']['group'] = t('redbubble');
  $data['rb_products']['table']['base'] = array(
    'field' => 'aid',
    'title' => t('redbubble'),
    'help' => t("redbubble product data for drupal"),
    'weight' => -10,
  );

  $data['rb_products']['aid'] = array(
    'title' => t('aid'),
    'help' => t('ID number for redbubble product.'),
    'field' => array(
      'group' => t('redbubble'),
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['rb_products']['title'] = array(
    'title' => t('The name of the product'),
    'help' => t('The name of the product'),
    'field' => array(
      'group' => t('redbubble'),
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['rb_products']['description'] = array(
    'title' => t('Description of the product'),
    'help' => t('Description of the product'),
    'field' => array(
      'group' => t('redbubble'),
      'handler' => 'views_handler_field_markup',
      'format' => 'full_html',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_field_markup',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['rb_products']['popnum'] = array(
    'title' => t('Popularity number'),
    'help' => t('Popularity number'),
    'field' => array(
      'group' => t('redbubble'),
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['rb_products']['imageurl'] = array(
    'title' => t('product image url'),
    'help' => t('product image url'),
    'field' => array(
      'group' => t('redbubble'),
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['rb_products']['productid'] = array(
    'title' => t('Product ID number'),
    'help' => t('Product ID number'),
    'field' => array(
      'group' => t('redbubble'),
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );


  $data['rb_products']['price'] = array(
    'title' => t('The price of the product'),
    'help' => t('The price of the product'),
    'field' => array(
      'group' => t('redbubble'),
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['rb_products']['pubdate'] = array(
    'title' => t('Publication Date'),
    'help' => t('Publication Date'),
    'field' => array(
      'group' => t('redbubble'),
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );


  $data['rb_products']['artist'] = array(
    'title' => t('Artist name'),
    'help' => t('Artist name'),
    'field' => array(
      'group' => t('redbubble'),
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['rb_products']['sales_link'] = array(
    'title' => t('Link to location where this product can be boutgh.'),
    'help' => t('Link to location where this product can be boutgh.'),
    'field' => array(
      'group' => t('redbubble'),
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['rb_products']['color'] = array(
    'title' => t('color of the product'),
    'help' => t('color of the product'),
    'field' => array(
      'group' => t('redbubble'),
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['rb_products']['size'] = array(
    'title' => t('Size of product'),
    'help' => t('Size of product'),
    'field' => array(
      'group' => t('redbubble'),
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );


  $data['rb_products']['ptype'] = array(
    'title' => t('Product type'),
    'help' => t('Product type'),
    'field' => array(
      'group' => t('redbubble'),
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['rb_products']['kwords'] = array(
    'title' => t('Keywords of this product'),
    'help' => t('Keywords of this product'),
    'field' => array(
      'group' => t('redbubble'),
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['rb_products']['custom'] = array(
    'title' => t('Can this product be customized'),
    'help' => t('Can this product be customized'),
    'field' => array(
      'group' => t('redbubble'),
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );


  $data['rb_products']['style'] = array(
    'title' => t('Style of the product'),
    'help' => t('Style of the product'),
    'field' => array(
      'group' => t('redbubble'),
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['rb_products']['prodnumber'] = array(
    'title' => t('Product Numnber'),
    'help' => t('Product Number'),
    'field' => array(
      'group' => t('redbubble'),
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['rb_products']['created'] = array(
    'title' => t('The Unix timestamp when the record was created.'),
    'help' => t('The Unix timestamp when the record was created.'),
    'field' => array(
      'group' => t('redbubble'),
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  return $data;
}

